# class User:
#     def __init__(self, first_name, last_name, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age
#         self.city = "Mumbai"

#     def greet(self):
#         return f"Hello my name is {self.first_name} {self.last_name} and I am {self.age} years old."

#     def __repr__(self):
#         return (
#             "-*-" * 10
#             + "\n"
#             + f"Name: {self.first_name} {self.last_name}\nAge: {self.age}"
#             + "\n"
#             + "-*-" * 10
#             + "\n"
#         )


# john = User("John", "Doe", 20)
# jane = User("Jane", "Smith", 24)

# print(john)
# print(jane)


# class User:
#     def __init__(self, first_name, last_name, age, email, password):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age
#         self._email = email
#         self._password = password
#         self.city = "Mumbai"

#     def greet(self):
#         return f"Hello my name is {self.first_name} {self.last_name} and I am {self.age} years old."

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name}"

#     def login(self, password):
#         if self._password == password:
#             print(f"{self._email} has just logged in")
#             return True

#         raise ValueError("Incorrect password")

#     def logout(self):
#         print(f"{self._email} has logged out")
#         return True


# class Admin(User):
#     def __repr__(self):
#         return f"{self.first_name} {self.last_name} (ADMIN)"

#     def create_group(self, group_name):
#         print(f"{group_name} group created by {self.email}")


# john = User("John", "Doe", 20, "john@gmail.com", "john123")
# jane = Admin("Jane", "Smith", 24, "jane@smith.com", "jane123")

# print(john.create_group("Python"))
# print(jane.greet())
# jane.create_group("Python")

# print(john)
# print(jane)


# class User:
#     def __init__(self, first_name, last_name, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self._age = age

#     def greet(self):
#         return f"Hello my name is {self.first_name} {self.last_name} and I am {self._age} years old."

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name}"

#     @property
#     def age(self):
#         return self._age

#     @age.setter
#     def age(self, new_age):
#         if new_age < 18 or new_age > 90:
#             raise ValueError("Please enter a valid age")

#         self._age = new_age
#         return self._age

#     # def get_age(self):
#     #     return self._age

#     # def set_age(self, new_age):
#     #     if new_age < 18 or new_age > 90:
#     #         raise ValueError("Please enter a valid age")

#     #     self._age = new_age
#     #     return self._age


# john = User("John", "Doe", 20)

# john.age = 30
# print(john.age)

# # john.set_age(25)
# # print(john.get_age())


# class User:
#     def __init__(self, first_name, last_name, age, email, password):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age
#         self._email = email
#         self._password = password
#         self.city = "Mumbai"

#     def greet(self):
#         return f"Hello my name is {self.first_name} {self.last_name} and I am {self.age} years old."

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name}"

#     def login(self, password):
#         if self._password == password:
#             print(f"{self._email} has just logged in")
#             return True

#         raise ValueError("Incorrect password")

#     def logout(self):
#         print(f"{self._email} has logged out")
#         return True


# class User:
#     def __init__(self, first_name, last_name, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age
#         self.city = "Mumbai"

#     def greet(self):
#         return f"Hello my name is {self.first_name} {self.last_name} and I am {self.age} years old."

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name}"


# class Admin(User):
#     def __init__(self, first_name, last_name, age, email, password):
#         super().__init__(first_name, last_name, age)
#         self.email = email
#         self._password = password

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name} (ADMIN)"

#     def create_group(self, group_name):
#         print(f"{group_name} group created by {self.email}")


# john = User("John", "Doe", 20)
# jane = Admin("Jane", "Smith", 24, "jane@smith.com", "jane123")


# print(jane.email)


class Human:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __repr__(self):
        return f"{self.name}\nAge: {self.age}"

    def greet(self):
        return "Hello World"

    def __len__(self):
        return self.age

    def __lt__(self, other_human):
        return self.age < other_human.age

    def __gt__(self, other_human):
        return self.age > other_human.age

    def __add__(self, other_human):
        if isinstance(other_human, Human):
            return Human("New born", 0)

        raise ValueError("Humans can only be added to other human you sick @#$@#$")


class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def greet(self):
        return "sdjklnfasjkldfnljkasdf"


class Mutant(Animal, Human):
    pass


# scott = Mutant("Scott Summers", 30)

john = Human("John Doe", 20)
jane = Human("Jane Smith", 30)
tom = Animal("Tom Doe", 6)

print(john + tom)
# print(isinstance(tom, Human))


# print(john > jane)


# print(john.greet())
# print(tom.greet())
# print(scott.greet())

# help(scott)

# print(len(john))
