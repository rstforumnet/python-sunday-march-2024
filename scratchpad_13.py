# # # # # for word = next(arr1) in arr1 = iter(arr1):
# # # # #     print(word)


# # # # # def my_for(iterable):
# # # # #     iterator = iter(iterable)

# # # # #     while True:
# # # # #         try:
# # # # #             print(next(iterator))
# # # # #         except StopIteration:
# # # # #             break


# # # # # arr1 = ["hello", "world", "jack", "jane", "john"]

# # # # # my_for(arr1)

# # # # # # for word in arr1:
# # # # #     print(word)

# # # # # r = range(0, 10)

# # # # # ir = iter(r)
# # # # # print(next(ir))
# # # # # print(next(ir))
# # # # # print(next(ir))


# # # # # class Counter:
# # # # #     def __init__(self, start, end, step=1):
# # # # #         self.start = start
# # # # #         self.end = end
# # # # #         self.step = step

# # # # #     def __repr__(self):
# # # # #         return f"Counter({self.start}, {self.end})"

# # # # #     def __iter__(self):
# # # # #         return self

# # # # #     def __next__(self):
# # # # #         if self.start < self.end:
# # # # #             num = self.start
# # # # #             self.start += self.step
# # # # #             return num
# # # # #         else:
# # # # #             raise StopIteration


# # # # # r = range(0, 10, 2)
# # # # # c = Counter(0, 10, 2)

# # # # # for num in c:
# # # # #     print(num)


# # # # # def counter(start, end, step=1):
# # # # #     while start < end:
# # # # #         yield start
# # # # #         start += step


# # # # # c = counter(0, 10, 2)

# # # # # for num in c:
# # # # #     print(num)


# # # # def fib_list(max):
# # # #     nums = []
# # # #     a, b = 0, 1
# # # #     while len(nums) < max:
# # # #         nums.append(a)
# # # #         a, b = b, a + b
# # # #     return nums


# # # # def fib_gen(max):
# # # #     a, b = 0, 1
# # # #     count = 0
# # # #     while count < max:
# # # #         a, b = b, a + b
# # # #         yield a
# # # #         count += 1


# # # # nums_list = fib_list(100)
# # # # nums_gen = fib_gen(100)

# # # # print(nums_gen)

# # # # # for num in nums_list:
# # # # #     print(num)

# # # from time import time

# # # list_start_time = time()
# # # print(sum([num for num in range(100000000)]))
# # # list_stop = time() - list_start_time

# # # gen_start_time = time()
# # # print(sum(num for num in range(100000000)))
# # # gen_stop = time() - gen_start_time

# # # print(f"List Comp took: ", list_stop)
# # # print(f"Gen took: ", gen_stop)


# # # def add(a, b):
# # #     return a + b


# # # def sub(a, b):
# # #     return a - b


# # # def math(a, b, fn):
# # #     return fn(a, b)


# # # print(math(10, 20, sub))


# # def sum(num, func):
# #     total = 0
# #     for n in range(1, num + 1):
# #         total += func(n)
# #     return total


# # def square(x):
# #     return x**2


# # print(sum(10, square))


# import random


# # def greet(person):
# #     def get_mood():
# #         mood = ["Hey", "What!", "What the heck do you want!", "Get lost!"]
# #         msg = random.choice(mood)
# #         return msg

# #     result = f"{get_mood()} {person}"
# #     return result


# # print(greet("John"))


# def make_greet_func(person):
#     def make_message():
#         mood = ["Hey", "What!", "What the heck do you want!", "Get lost!"]
#         msg = random.choice(mood)
#         return f"{msg} {person}"

#     return make_message


# result1 = make_greet_func("John")
# result2 = make_greet_func("Jane")

# print(result1())
# print(result2())

import uuid
import datetime


class Company:
    def __init__(self, name, address, phone_nos, email, company_registration_code):
        self.name = name
        self.address = address
        self.phone_nos = phone_nos
        self.email = email
        self.company_registration_code = company_registration_code
        self.employees = []

    def __repr__(self):
        return self.name

    def create_employee(
        self,
        first_name,
        middle_name,
        last_name,
        address,
        aadhar_card_no,
        pan_card_no,
        phone,
        email,
    ):
        if not all(
            [
                first_name,
                middle_name,
                last_name,
                address,
                aadhar_card_no,
                pan_card_no,
                phone,
                email,
            ]
        ):
            raise ValueError("All fields are required to create an employee")

        new_employee = Employee(
            first_name,
            middle_name,
            last_name,
            address,
            aadhar_card_no,
            pan_card_no,
            phone,
            email,
        )
        self.employees.append(new_employee)
        return new_employee

    def find_employee(self, id):
        for employee in self.employees:
            if employee.employee_id == id:
                return employee
        raise ValueError("Employee not found")

    def filter_employee(self, name):
        filtered = [
            employee for employee in self.employees if name in employee.get_full_name()
        ]
        if not filtered:
            raise ValueError("No employee found with this name")
        return filtered


class Employee:
    def __init__(
        self,
        first_name,
        middle_name,
        last_name,
        address,
        aadhar_card_no,
        pan_card_no,
        phone,
        email,
    ):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.address = address
        self.aadhar_card_no = aadhar_card_no
        self.pan_card_no = pan_card_no
        self.phone = phone
        self.email = email
        self.employee_id = str(uuid.uuid4()).split("-")[0]
        self.attendance = []

    def __repr__(self):
        return f"{self.first_name} {self.middle_name} {self.last_name}"

    def get_full_name(self):
        return f"{self.first_name} {self.middle_name} {self.last_name}"

    def get_details(self):
        return {
            "first_name": self.first_name,
            "middle_name": self.middle_name,
            "last_name": self.last_name,
            "address": self.address,
            "aadhar_card_no": self.aadhar_card_no,
            "pan_card_no": self.pan_card_no,
            "email": self.email,
            "phone": self.phone,
            "employee_id": self.employee_id,
        }

    def edit_details(
        self,
        first_name=None,
        middle_name=None,
        last_name=None,
        address=None,
        phone=None,
        email=None,
    ):
        if first_name:
            self.first_name = first_name
        if middle_name:
            self.middle_name = middle_name
        if last_name:
            self.last_name = last_name
        if address:
            self.address = address
        if phone:
            self.phone = phone
        if email:
            self.email = email

    def punch_in(self):
        self.attendance.append({"date": datetime.now(), "punch": "in"})

    def punch_out(self):
        self.attendance.append({"date": datetime.now(), "punch": "out"})
