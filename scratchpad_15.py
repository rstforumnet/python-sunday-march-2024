# # # import pickle
# # import json
# # import jsonpickle


# class User:
#     def __init__(self, first_name, last_name, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name}"

#     def greet(self):
#         return "Hello world!"


# # with open("users_data2.json") as file:
# #     contents = file.read()
# #     users = jsonpickle.decode(contents)
# #     print(users[0].greet())

# # # user1 = User("John", "Doe", 20)
# # # user2 = User("Jane", "Smith", 30)

# # # user2.first_name = "Richard"

# # # with open("users_data2.json", "w") as file:
# # #     json_data = jsonpickle.encode((user1, user2))
# # #     file.write(json_data)

# # # with open("users_data1.json", "w") as file:
# # #     file.write(json.dumps(user1.__dict__))

# # # # print(user2.first_name)

# # # with open("user_data.pickle", "wb") as file:
# # #     pickle.dump([user1, user2], file)

# # # with open("user_data.pickle", "rb") as file:
# # #     data = pickle.load(file)
# # #     print(data[1].first_name)

import re

# pattern = re.compile(r"[a-zA-Z0-9-_\.\+]+@[a-zA-Z0-9-]+\.[a-zA-Z\.]+")

# # result = pattern.search("Email us today at support@something.com or info@something.com")
# # print(result.group())

# result = pattern.findall(
#     "Email us today at support@something.com or info@something.com"
# )
# print(result)


# def extract_email(text):
#     email_pattern = re.compile(r"[a-zA-Z0-9-_\.\+]+@[a-zA-Z0-9-]+\.[a-zA-Z\.]+")
#     match = email_pattern.search(text)
#     if match:
#         return match.group()
#     return None


# print(extract_email("Email us toda"))


# def is_valid_email(email_address):
#     email_pattern = re.compile(r"^[a-zA-Z0-9-_\.\+]+@[a-zA-Z0-9-]+\.[a-zA-Z\.]+$")
#     match = email_pattern.search(email_address)
#     if match:
#         return True
#     return False


def is_valid_email(email_address):
    email_pattern = re.compile(r"[a-zA-Z0-9-_\.\+]+@[a-zA-Z0-9-]+\.[a-zA-Z\.]+")
    match = email_pattern.fullmatch(email_address)
    if match:
        return True
    return False


print(is_valid_email("support@something.com"))
