# html = """
# <html>
# 	<head>
# 		<title>My Web Page</title>
# 	</head>
# 	<body>
# 		<h4>This is my app</h4>
# 		<div>
# 			<h1 id="title">Hello World</h1>
# 			<h2 class="hello">Hello World</h2>
# 			<ul>
# 				<li class="hello">
# 					Lorem ipsum dolor, <strong>sit amet consectetur</strong> adipisicing.
# 				</li>
# 				<li>Lorem ipsum dolor, sit amet consectetur adipisicing.</li>
# 				<li class="hello">
# 					Lorem ipsum dolor, sit amet consectetur adipisicing.
# 				</li>
# 				<li>Lorem ipsum dolor, sit amet consectetur adipisicing.</li>
# 			</ul>
# 			<p>
# 				Lorem ipsum dolor sit, <strong>amet consectetur</strong> adipisicing
# 				elit. Vitae iste nisi voluptatem temporibus officiis accusamus tempore
# 				repellat doloribus nihil labore ab earum fuga, necessitatibus, id
# 				aperiam ducimus rerum nesciunt doloremque.
# 			</p>
# 			<p>
# 				<a href="https://google.com">Go to google</a>
# 				<img src="" class="hello" />
# 			</p>
# 		</div>
# 	</body>
# </html>
# """

# from bs4 import BeautifulSoup

# soup = BeautifulSoup(html, "html.parser")

# # print(type(soup))
# # print(soup.body.h4)

# # print(soup.find_all("p"))

# # print(soup.find(id="title"))
# # print(soup.find_all(class_="hello"))

# # print(soup.find("h1").get_text())

# print(soup.find("a").get_text())
# print(soup.find("a")["href"])

# import requests
# from bs4 import BeautifulSoup
# from csv import writer

# response = requests.get("https://arstechnica.com/")

# soup = BeautifulSoup(response.text, "html.parser")

# articles = soup.find_all(class_="article")

# with open("articles.csv", "w", newline="") as file:
#     csv_writer = writer(file)
#     csv_writer.writerow(["Title", "Excerpt", "Author", "Date", "Link"])

#     for article in articles:
#         title = article.find("h2").get_text()
#         excerpt = article.find(class_="excerpt").get_text()
#         author = article.find("span").get_text()
#         date = article.find("time").get_text()
#         link = article.find("a")["href"]

#         csv_writer.writerow([title, excerpt, author, date, link])


import requests
from bs4 import BeautifulSoup
from pprint import pprint

response = requests.get("https://quotes.toscrape.com/")

all_quotes = []
base_url = "https://quotes.toscrape.com"
url = "/page/1/"

while url:
    response = requests.get(f"{base_url}{url}")
    print(f"Now scraping {base_url}{url}")
    soup = BeautifulSoup(response.text, "html.parser")
    quotes = soup.find_all(class_="quote")

    for quote in quotes:
        all_quotes.append(
            {
                "text": quote.find(class_="text").get_text(),
                "author": quote.find(class_="author").get_text(),
                "author": quote.find("a")["href"],
            }
        )

    next_btn = soup.find(class_="next")
    if next_btn:
        url = next_btn.find("a")["href"]
    else:
        url = None


pprint(all_quotes)
