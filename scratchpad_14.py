# # # # def stars(fn):
# # # #     def wrapper():
# # # #         print("-*-" * 10)
# # # #         fn()
# # # #         print("-*-" * 10)

# # # #     return wrapper


# # # # @stars
# # # # def say_hello():
# # # #     print("Hello world")


# # # # # say_hello = stars(say_hello)

# # # # say_hello()


# # # # # @classname # hello = classname(hello)
# # # # # def hello():
# # # # #     pass

# # # from functools import wraps


# # # def make_upper_case(fn):
# # #     """Decorator function that uppercases the return value"""

# # #     @wraps(fn)
# # #     def wrapper(*args, **kwargs):
# # #         """Wrapper function"""
# # #         return fn(*args, **kwargs).upper()

# # #     return wrapper


# # # @make_upper_case  # say_hello = make_upper_case(say_hello)
# # # def say_hello():
# # #     """Function that prints hello world"""
# # #     return "Hello World"


# # # @make_upper_case  # say_hello_to_person = make_upper_case(say_hello_to_person)
# # # def say_hello_to_person(person):
# # #     """Function that greets a person"""
# # #     return f"Hello {person}"


# # # # print(say_hello_to_person("John Doe"))
# # # # print(say_hello())

# # # print(say_hello_to_person.__name__)
# # # print(say_hello_to_person.__doc__)

# # # print(say_hello.__name__)
# # # print(say_hello.__doc__)


# # # file = open("hello.txt")
# # # contents = file.read()
# # # file.close()


# # # with open("hello.txt") as file:
# # #     contents = file.read()
# # #     print(contents)

# # # print(file.closed)


# # with open("hello2222.txt", "a") as file:
# #     file.write("-*-" * 10)
# #     file.write("\n")
# #     file.write("Hello World!!!!!!!\n")
# #     file.write("This is something\n")
# #     file.write("-*-" * 10)
# #     file.write("\n")


# # # with open("hello.txt", "r+") as file:
# # #     file.seek(0)
# # #     file.write("############################")

# # # with open("hello.txt") as file:
# # #     contents = file.read()

# # # contents = contents.replace("Hello", "Namaste")

# # # with open("hello.txt", "w") as file:
# # #     file.write(contents)


# # from csv import reader, DictReader
# # from pprint import pprint

# # with open("hello.csv") as file:
# #     contents = reader(file)
# #     pprint(list(contents))

# # print()

# # with open("hello.csv") as file:
# #     contents = DictReader(file)
# #     pprint(list(contents))


# # from csv import writer

# # with open("hello2.csv", "w", newline="") as file:
# #     csv_writer = writer(file)
# #     csv_writer.writerow(["Full Name", "Age", "Email", "Phone No."])
# #     csv_writer.writerow(["Full Name", "Age", "Email", "Phone No."])
# #     csv_writer.writerow(["Full Name", "Age", "Email", "Phone No."])
# #     csv_writer.writerow(["Full Name", "Age", "Email", "Phone No."])
# #     csv_writer.writerow(["Full Name", "Age", "Email", "Phone No."])


# from csv import DictWriter

# with open("hello3.csv", "w", newline="") as file:
#     headers = ["Full Name", "Age", "Email"]
#     csv_writer = DictWriter(file, fieldnames=headers)
#     csv_writer.writeheader()
#     csv_writer.writerow(
#         {"Full Name": "John Doe", "Age": 20, "Email": "john.doe@gmail.com"}
#     )


##### TASK

# Bank (bank.py)
# Attributes: name, initials, address, branch, ifsc_code, micr_code, phone_nos, employees: list[Employee], customers: List[Customer]
# Methods: get_details, edit_details(), get_customers_list(), find_customer_by_account_no(), filter_customer_by_name(), find_customer_by_email(), find_customer_by_phone(), delete_customer(), export_customers_as_csv() [SAME CRUD OPS FOR EMPLOYEE]

# Customer (customer.py)
# Attributes: first_name (2 chars), middle_name (2 char), last_name (2 char), phone, email, date_of_birth, aadhar_card_no, pan_card_no, customer_id, account_no, balance (5000), account_type
# Methods: get_full_name(), get_details(), get_age() edit_details(), get_balance(), deposit(), withdraw()

# Employee (employee.py)
# Attributes: first_name, middle_name, last_name, phone, email, date_of_birth, aadhar_card_no, pan_card_no, employee_id, job_title, salary, attendance
# Methods: get_full_name(), get_details(), edit_details(), punch_in(), punch_out()


# class Helper:
#     @classmethod
#     def is_valid_email(cls): ...


# class User:
#     def __init__(self, name, email, phone):
#         if not is_valid_email(email):
#             raise ValueError("Please enter a valid email")

#         self.name = name
#         self.email = email


# day/month/year (dd/mm/YYYY)


# -----------------------------

# rev_str("hello") -> "olleh"
# is_palindrome("Kayak") -> False

# Kayak
