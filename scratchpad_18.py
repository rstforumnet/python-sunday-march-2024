# # import sqlite3

# # conn = sqlite3.connect("users_db.sqlite")
# # cursor = conn.cursor()

# # # cursor.execute(
# # #     """
# # #     CREATE TABLE users (
# # #         id INTEGER PRIMARY KEY,
# # #         name TEXT,
# # #         email TEXT UNIQUE,
# # #         age INTEGER
# # #     )
# # #     """
# # # )

# # # user1 = ("John Doe", "john@gmail.com", "20")

# # # cursor.execute(f"INSERT INTO USERS (name, email, age) VALUES (?, ?, ?)", user1)

# # # users = [
# # #     ("John Doe", "johnd@gmail.com", 23),
# # #     ("Jane Smith", "jane@gmail.com", 24),
# # #     ("Jack Dee", "jackdee@gmail.com", 30),
# # #     ("John Dee", "johndee@gmail.com", 45),
# # # ]

# # # for user in users:
# # #     print(f"Writing {user[0]} to database...")
# # #     cursor.execute(f"INSERT INTO USERS (name, email, age) VALUES (?, ?, ?)", user)

# # cursor.execute("SELECT * FROM users;")
# # all_users = cursor.fetchall()

# # for user in all_users:
# #     print(user)

# # conn.commit()
# # conn.close()

# import pymongo

# client = pymongo.MongoClient()
# my_python_users_db = client["my_python_users_db"]
# users = my_python_users_db["users"]

# # dummy_users = [
# #     {"name": "John Doe", "email": "john.doe@gmail.com", "age": 30},
# #     {"name": "John Doe", "email": "john.doe@gmail.com", "age": 30},
# #     {"name": "John Doe", "email": "john.doe@gmail.com", "age": 30},
# #     {"name": "John Doe", "email": "john.doe@gmail.com", "age": 30},
# #     {"name": "John Doe", "email": "john.doe@gmail.com", "age": 30},
# # ]

# # users.insert_many(dummy_users)

# # users.update_many({"name": "John Doe"}, {"$set": {"phone": "+91 9827654321"}})

# users.delete_many({"name": "John Doe"})

# from flask import Flask

# app = Flask(__name__)


# @app.route("/", methods=["GET"])
# def home():
#     return "API is running..."


# if __name__ == "__main__":
#     app.run()


import sqlite3

conn = sqlite3.connect("users_gui.sqlite")
cursor = conn.cursor()

cursor.execute(
    """
    CREATE TABLE users (
        id INTEGER PRIMARY KEY,
        name TEXT,
        email TEXT UNIQUE,
        phone TEXT
    )
    """
)

conn.commit()
conn.close()
