# # # import random

# # # SUIT_TUPLE = ("Spades", "Hearts", "Clubs", "Diamonds")
# # # RANK_TUPLE = (
# # #     "Ace",
# # #     "2",
# # #     "3",
# # #     "4",
# # #     "5",
# # #     "6",
# # #     "7",
# # #     "8",
# # #     "9",
# # #     "10",
# # #     "Jack",
# # #     "Queen",
# # #     "King",
# # # )

# # # NCARDS = 8


# # # def get_card(deck_list_in):
# # #     this_card = deck_list_in.pop()
# # #     return this_card


# # # def shuffle(deck_list_in):
# # #     deck_list_out = deck_list_in.copy()
# # #     random.shuffle(deck_list_out)
# # #     return deck_list_out


# # # print("Welcome to higher or lower.")
# # # print(
# # #     "You have to choose whether the next card to be shown will be higher or lower than the current card."
# # # )
# # # print("Getting it right adds 20 points; get it wrong and you will lose 15 points.")
# # # print("You have 50 points to start.")
# # # print()


# # # starting_deck_list = []
# # # for suit in SUIT_TUPLE:
# # #     for this_value, rank in enumerate(RANK_TUPLE):
# # #         card_dict = {"rank": rank, "suit": suit, "value": this_value + 1}
# # #         starting_deck_list.append(card_dict)


# # # score = 50

# # # while True:
# # #     print()
# # #     game_deck_list = shuffle(starting_deck_list)
# # #     current_card_dict = get_card(game_deck_list)

# # #     current_card_rank = current_card_dict["rank"]
# # #     current_card_suit = current_card_dict["suit"]
# # #     current_card_value = current_card_dict["value"]

# # #     print(f"Starting card is {current_card_rank} of {current_card_suit}")
# # #     print()

# # #     for card_number in range(0, NCARDS):
# # #         answer = input(
# # #             f"Will the next card be higher or lower than the {current_card_rank} of {current_card_suit}? (enter h or l): "
# # #         )
# # #         answer = answer.lower()

# # #         next_card_dict = get_card(game_deck_list)
# # #         next_card_rank = next_card_dict["rank"]
# # #         next_card_suit = next_card_dict["suit"]
# # #         next_card_value = next_card_dict["value"]

# # #         print(f"Next card is {next_card_rank} of {next_card_suit}")

# # #         if answer == "h":
# # #             if next_card_value > current_card_value:
# # #                 print("You got it right, it was higher")
# # #                 score += 20
# # #             else:
# # #                 print("Sorry, it was not higher")
# # #                 score -= 15
# # #         elif answer == "l":
# # #             if next_card_value < current_card_value:
# # #                 print("You got it right, it was lower")
# # #                 score += 20
# # #             else:
# # #                 print("Sorry, it was not lower")
# # #                 score -= 15

# # #         print(f"Your score is: {score}")
# # #         print()

# # #         current_card_rank = next_card_rank
# # #         current_card_suit = next_card_suit
# # #         current_card_value = next_card_value

# # #     go_again = input("To play again, press ENTER, or 'q' to quit: ")
# # #     if go_again == "q":
# # #         break

# # # print("Thank you for playing")


# # total = 0


# # def increase(val):
# #     new_value = val + 1
# #     return new_value


# # total = increase(total)
# # print(increase(total))

# # # def increase():
# # #     global total
# # #     total += 1
# # #     print(total)


# # # increase()
# # # increase()
# # # increase()
# # # increase()
# # # increase()
# # # increase()
# # # increase()

# # "hello world" => strtoupper("hello world")

# # "hello world".upper()


# def make_user(first_name, last_name, age, email, password):
#     return {
#         "first_name": first_name,
#         "last_name": last_name,
#         "age": age,
#         "email": email,
#         "password": password,
#     }


# def user_greet(user):
#     return f"Hello, my name is {user["first_name"]} {user["last_name"]} and I am {user["age"]} years old"


# # john = make_user("John", "Doe", 20, "john.doe@hello.com", "john123")
# # print(user_greet(john))


# # def user(first_name, last_name, age, email, password):
# #     def greet():
# #         return f"Hello, my name is {first_name} {last_name} and I am {age} years old"

# #     user_dict = {
# #         "first_name": first_name,
# #         "last_name": last_name,
# #         "age": age,
# #         "email": email,
# #         "password": password,
# #         "greet": greet,
# #     }

# #     return user_dict


# # john = user("John", "Doe", 20, "john.doe@hello.com", "john123")
# # jane = user("Jane", "Smith", 30, "jane.smith@hey.com", "jane123")

# # print(john["greet"]())
# # print(jane["greet"]())

# # print(type(john))
# # print(type(jane))


# class User:
#     def __init__(self, first_name, last_name, age, email, phone):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age
#         self.email = email
#         self.phone = phone

#     def greet(self):
#         return f"Hello, my name is {self.first_name} {self.last_name} and I am {self.age} years old"


# jack = User("Jack", "Roe", 20, "jack.doe@hello.com", "jack123")
# jill = User("Jill", "Smith", 30, "jill.smith@hey.com", "jill123")

# print(jack.greet())
# print(jill.greet())

# print(type(jack))
# print(type(jill))


class User:
    def __init__(self, first_name, ln, age):
        self.first_name = first_name
        self.last_name = ln
        self.age = age
        self.city = "Mumba i"


user1 = User("John", "Doe", 20)
user2 = User("Jane", "Smith", 25)

print(user1.first_name)
print(user2.first_name)
