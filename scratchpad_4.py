# age = input("Please enter your age: ")

# if age:
#     age = int(age)

#     if age >= 18 and age < 21:
#         print("You can enter, but you cannot drink")
#     elif age >= 21 and age < 65:
#         print("You can enter and you can drink")
#     elif age >= 65:
#         print("Drinks are free")
#     else:
#         print("You are not allowed to enter")
# else:
#     print("Please enter a value.")


# for char in range(100, 200):
#     print(char)
#     print("--------------------")


# for num in range(0, 10):
#     print(num)


# for num in range(0, 21, 10):
#     print(num)

# for num in range(10, -1, -2):
#     print(num)

# for num in range(1, 21):
#     if num == 5 or num == 16:
#         print("FizzBuzz")
#     elif num % 2 == 0:
#         print(num, "Fizz is even")
#     elif num % 2 == 1:
#         print(num, "Fizz is odd")


# password = input("Please enter the secret password: ")

# while password != "world":
#     if password == "quit":
#         break
#     print("Incorrect password")
#     password = input("Please try again: ")


# password = input("Please enter the secret password: ")

# while True:
#     if password == "world":
#         break
#     print("Incorrect password")
#     password = input("Please try again: ")


langs = ["Python", "JavaScript", "Rust", "Elm", "WASM"]

for lang in langs:
    print(lang)

# index = 0
# while index < len(langs):
#     print(langs[index])
#     index += 1
