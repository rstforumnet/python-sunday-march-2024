# def greet():
#     print("Hello World")
#     print("Hello Again")


# greet()
# greet()

# from random import random


# def flip_coin():
#     num = random()
#     if num > 0.5:
#         print("HEADS")
#     else:
#         print("TAILS")


# flip_coin()
# flip_coin()
# flip_coin()


# def greet():
#     print("Something")
#     return "Hello World"


# print(greet())
# result = greet()
# print(result)

# result = type(10)
# print(result)


# def greet(name, message):
#     return f"{message}, {name}"


# print(greet("John Doe", "Hi"))


# def add(a, b):
#     return a + b


# print(add("10", "20"))


# def sum(numbers):
#     total = 0
#     for num in numbers:
#         if num % 2 != 0:
#             total += num
#     return total


# print(sum([1, 2, 3, 4, 5, 6, 7, 8, 9]))


# def is_odd_num(number):
#     if number % 2 != 0:
#         return True
#     return False


# print(is_odd_num(21))

# "hello world this is something" => "Hello World This Is Something"
# .title()


# words = "hello world this is something"

# words_split = words.split()

# capitalize_words = []

# for word in words_split:
#     capitalize_words.append(word.capitalize())

# print(" ".join(capitalize_words))


# def capitalize_words(words):
#     words_split = words.split()
#     capitalize_words = []
#     for word in words_split:
#         capitalize_words.append(word.capitalize())
#     return " ".join(capitalize_words)


# def capitalize_words(words):
#     return " ".join(word.capitalize() for word in words.split())


# print(capitalize_words("hello world this is something"))
# print(capitalize_words("hello world"))


# def add(a=0, b=0):
#     return a + b


# print(add())


# from random import random


# def flip_coin():
#     num = random()
#     if num > 0.5:
#         print("HEADS")
#     else:
#         print("TAILS")


# def flip_multiple(times=1):
#     for _ in range(times):
#         flip_coin()


# flip_multiple(10)


# def add(a: int | float, b: int | float) -> int | float:
#     return a + b


# print(add(10))

# a = 10
# a = True


# def add(a, b):
#     a = float(a)
#     b = float(b)
#     return a + b


# # print(add(10, 20))
# print(add("hello", "world"))

# def add(a: int | float, b: int | float) -> int | float:
#     return a + b


# print(add(10, 20.2))


# def greet(name, message):
#     return f"{message}, {name}"


# your_greeting = input("Please enter your greeting: ")

# print(greet(message=your_greeting, name="Jack Roe"))

# full_name = "John Doe"


# def say_hello():
#     full_name = "Jack Roe"
#     print(full_name)


# say_hello()

# print(full_name)


# total = 0


# def dummy():
#     global total
#     total += 1
#     print(total)


# dummy()
# print(total)


# def outer():
#     count = 0

#     def inner():
#         nonlocal count
#         count += 1
#         print(count)

#     inner()


# # outer()
# from random import random


# def flip_coin():
#     """Function will return the string "HEADS" or "TAILS" randomly
#     Use it to do something
#     Another line
#     """
#     num = random()
#     if num > 0.5:
#         print("HEADS")
#     else:
#         print("TAILS")


# def flip_multiple(times=1):
#     """Function that will flip the coin several times
#     @param times {int}
#     @returns {int}
#     """
#     for _ in range(times):
#         flip_coin()


# # flip_coin()
# # print(flip_coin.__doc__)
# # print(flip_multiple.__doc__)

# # print([].append.__doc__)

# # help(flip_multiple)

# help([].append)
