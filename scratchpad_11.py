# song1 = {
#     "title": "Song #1",
#     "album": "Some album",
#     "artists": ["John Doe"],
#     "track_length": 4.52,
#     "label": "TestMusic Company",
#     "media_url": "https://...",
# }

# playlist1 = {
#     "title": "RnB",
#     "user_id": 3,
#     "songs": [
#         {
#             "title": "Song #1",
#             "album": "Some album",
#             "artists": ["John Doe"],
#             "track_length": 4.52,
#             "label": "TestMusic Company",
#             "media_url": "https://...",
#         },
#     ],
# }


# def add_song_to_playlist(playlist, song):
#     playlist["songs"].append(song)
#     return True


# add_song_to_playlist(playlist1, song1)


# class User:
#     total_users = 0

#     def __init__(self, first_name, last_name, email, phone, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.email = email
#         self.phone = phone
#         self._age = age
#         self.city = "Mumbai"
#         User.total_users += 1

#     @classmethod
#     def count_total_users(cls):
#         return cls.total_users

#     def greet(self):
#         return f"Hello, my name is {self.first_name} {self.last_name} and I am {self._age} years old."

#     def change_city(self, new_city_name):
#         self.city = new_city_name
#         return self.city

#     def get_age(self):
#         return self._age

#     def set_age(self, new_age):
#         if new_age < 18 or new_age > 80:
#             raise ValueError("Please enter a valid age")
#         self._age = new_age
#         return self._age


# user1 = User("John", "Doe", "john.doe@gmail.com", "+919876654321", 20)
# user2 = User("Jane", "Smith", "jane.smith@hey.com", "+918564434667", 24)


# print(User.count_total_users())


# print(user1.total_users)
# print(user2.total_users)
# print(User.total_users)


# user2.change_city("Delhi")
# print(user2.city)

# print(user2.greet())
# print(user1.greet())

# print(user1.first_name)

# user1.first_name = "Xi"

# print(user1.first_name)


# user1.set_age(30)
# print(user1.get_age())

# user1.age = 305678234
# print(user1.age)

# print(user1._age)
# print(user2._age)

# user1.email = "john@hey.com"
# print(user1.email)
# print(type(user1))


import random


class User:
    total_users = 0

    def __init__(self, first_name, last_name, email, phone, age):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone = phone
        self._age = age
        self.city = "Mumbai"
        User.total_users += 1

    @classmethod
    def generate_password(cls, length=8):
        chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-=_+[]';"
        password = ""
        for _ in range(length):
            password += random.choice(chars)
        return password

    @classmethod
    def count_total_users(cls):
        return cls.total_users

    def greet(self):
        return f"Hello, my name is {self.first_name} {self.last_name} and I am {self._age} years old."

    def change_city(self, new_city_name):
        self.city = new_city_name
        return self.city

    def get_age(self):
        return self._age

    def set_age(self, new_age):
        if new_age < 18 or new_age > 80:
            raise ValueError("Please enter a valid age")
        self._age = new_age
        return self._age


jane = User("Jane", "Smith", "janesmith@gmail.com", "+919876654321", 20)

# print(jane.get_age())

# print(User.generate_password())
print(User.count_total_users())


# john = User("John", "Smith", "john.smith@gmail.com", "+919876654321", 20)


# # print(User.count_total_users())

# print(User.generate_password(24))


# class Math:
#     @classmethod
#     def add(cls, a, b):
#         return a + b

#     @classmethod
#     def sub(cls, a, b):
#         return a - b

#     @classmethod
#     def mul(cls, a, b):
#         return a * b

#     @classmethod
#     def div(cls, a, b):
#         return a / b


# print(Math.add(10, 20))

# import my_math

# print(my_math.add(10, 20))
